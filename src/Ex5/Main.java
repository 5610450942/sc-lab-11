package Ex5;

public class Main {
	public static void main(String[] args){
		Refrigerator rf = new Refrigerator(5);
		try{
			rf.put("Coke1");
			rf.put("Coke2");
			System.out.println("Before take out :"+rf);
			rf.takeOut("Coke1");
			System.out.println("After take out:"+rf);
			rf.put("Pepsi");
			rf.put("Fanta");
			rf.put("Milk");
			rf.put("Beef");
			rf.takeOut("Fish");
			System.out.println(rf);
			rf.put("Cookie");

		}
		catch(FullException e){
			System.err.println(e.getMessage());
		}
	}
}