
public class MyClass {
	public void methX() throws DataException{
		throw new DataException("DataException"); // key word new
	}
	public void methY() throws FormatException{
		throw new FormatException("FarmatException");
	}
	public static void main(String[] args){
		try{
			MyClass c = new MyClass();
			System.out.print("A");
			c.methX();
			System.out.print("B");
			c.methY();
			System.out.print("C");
			return;
		}catch(DataException e){
			System.out.print("D");
		}catch(FormatException e){
			System.out.print("E");
		} finally {
			System.out.print("F");
		}
		System.out.print("G");
		System.out.println("\n 2. ����� Exception : ABCF");
		System.out.println("3. Exception ���  methX : ADFG");
		System.out.println("4. Exception ���  methY : ABEFG");


	}
}
