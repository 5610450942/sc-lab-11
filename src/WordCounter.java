import java.util.HashMap;


public class WordCounter {
	private String message;
	private HashMap<String,Integer> map;
	public WordCounter(String message){
		this.message = message;
		map = new HashMap<String,Integer>();
	}
	public void count(){
	for(String s : message.split(" ")){
		if(!map.containsKey(s)){
		map.put(s,1);
		}
		else{
			int a = map.get(s);
			map.replace(s, a, ++a);
		}
	}
	}
	public int hasWord(String word){
		if(map.containsKey(word)){
		return map.get(word);}
		else{
			return 0;
		}
	}
	public static void main(String[] args){
		WordCounter w = new WordCounter("ni ni ni ni c c k");
		w.count();
		System.out.print(w.hasWord("a"));
	}
}
